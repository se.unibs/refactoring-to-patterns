package it.unibs.ing.se.refactoring.retryandtrace;

public class Runner {
    public static void main(String[] args) {
        MetricsTracer tracer = new MetricsTracer();
        var service = new Service(tracer);
        var request = new ServiceRequest("Hello, World!");
        int executions = 3;
        for (int i = 0; i < executions; i++) {
            System.out.println("Running Execution # " + i + "...");
            try {
                var response = service.doSomething(request);
                System.out.println(response);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        System.out.printf("Average execution time: %.3f ms\n", tracer.executionAverageTime().getAsDouble());
    }
}
