package it.unibs.ing.se.refactoring.retryandtrace;

public record ServiceResponse(int value) {
}