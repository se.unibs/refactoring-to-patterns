package it.unibs.ing.se.refactoring.retryandtrace;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

public class MetricsTracer {
    private final List<Execution> executions = new ArrayList<>();

    public Execution startExecution() {
        Execution execution = new Execution(ZonedDateTime.now());
        executions.add(execution);
        return execution;
    }

    public OptionalDouble executionAverageTime() {
        return executions.stream().mapToDouble(x -> x.executionTime()).average();
    }
}
