package it.unibs.ing.se.refactoring.trafficlight;

/**
 * Inizialmente ROSSO (per le automobili)
 * Il ROSSO dura 10 cicli di clock (secondi)
 * Il VERDE dura 12 cicli di clock
 * Il GIALLO dura 3 cicli di clock
 *
 * La chiamata pedonale ha effetto esclusivamente quando il semaforo è verde: in tal caso il semaforo diventa GIALLO
 *  tre cicli di clock dopo la chiamata, a meno che non debba comunque diventare giallo prima. In questo caso la
 *  chiamata pedonale non ha effetto.
 *
 *  Possibile evoluzione futura: supportare la possibilità di estendere la durata del ROSSO qualora la chiamata pedonale
 *  avvenga quando il semaforo è già ROSSO.
 */
public class TrafficLight {
    private String color = "RED";
    private int changeIn = 10;

    public TrafficLight() {
        color = "RED";
        changeIn = 10;
    }

    public void clock() {
        changeIn--;
        if(changeIn == 0) {
            if("RED" == color) {
                color = "GREEN";
                changeIn = 12;
            } else if("GREEN" == color) {
                color = "YELLOW";
                changeIn = 3;
            } else if("YELLOW" == color) {
                color = "RED";
                changeIn = 10;
            }
        }
    }

    public void submitPedestrianCall() {
        if("GREEN" == color) {
            if(changeIn > 3) {
                changeIn = 3;
            }
        }
    }

    public String getColor() {
        return color;
    }
}
