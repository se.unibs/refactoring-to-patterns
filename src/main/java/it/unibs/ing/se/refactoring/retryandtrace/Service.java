package it.unibs.ing.se.refactoring.retryandtrace;

import java.util.Random;

public class Service {
    private static final Random Randomizer = new Random();
    private final MetricsTracer tracer;

    public Service(MetricsTracer tracer) {
        this.tracer = tracer;
    }

    public ServiceResponse doSomething(ServiceRequest request) throws Exception {
        int count = 5;
        int errorRatio = 5;
        var execution = tracer.startExecution();
        while(count > 0) {
            try {
                Thread.sleep(Randomizer.nextInt(2000));
                if(Randomizer.nextInt(errorRatio) < (errorRatio - 1)) {
                    throw new Exception("Horrible execution error!");
                }
                var response = new ServiceResponse(request.text() != null ? request.text().length() : 0);
                execution.complete();
                return response;
            } catch (Exception ex) {
                count--;
                if(count == 0) {
                    execution.completeWithError();
                    throw ex;
                } else {
                    System.out.printf("Exception %s Retry %d\n", ex.getMessage(), count);
                }
            }
        }
        return new ServiceResponse(-1);
    }
}
