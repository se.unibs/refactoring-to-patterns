package it.unibs.ing.se.refactoring.retryandtrace;

public record ServiceRequest(String text) {
}
