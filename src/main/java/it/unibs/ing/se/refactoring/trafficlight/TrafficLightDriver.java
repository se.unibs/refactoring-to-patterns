package it.unibs.ing.se.refactoring.trafficlight;

public class TrafficLightDriver {
    private static final TrafficLight light = new TrafficLight();
    public static void main(String[] args) {
        print("RED"); // RED
        letPassSeconds(2);
        print("RED"); // 2: RED
        letPassSeconds(2);
        print("RED"); // 4: RED
        letPassSeconds(8);
        print("GREEN"); // 12: GREEN
        letPassSeconds(6);
        print("GREEN"); // 18: GREEN
        letPassSeconds(5);
        print("YELLOW"); // 23: YELLOW
        letPassSeconds(1);
        print("YELLOW"); // 24: YELLOW
        letPassSeconds(1);
        print("RED"); // 25: RED
        letPassSeconds(10);
        print("GREEN"); // 35: GREEN
        callForGreen();
        letPassSeconds(2);
        print("GREEN"); // 37: GREEN
        letPassSeconds(1);
        print("YELLOW"); // 38: YELLOW
        letPassSeconds(3);
        print("RED"); // 41: RED
        letPassSeconds(10);
        print("GREEN"); // 51: GREEN
        letPassSeconds(10);
        print("GREEN"); // 61: GREEN
        callForGreen();
        letPassSeconds(2);
        print("YELLOW"); // 61: YELLOW
        letPassSeconds(3);
        print("RED"); // 61: RED
    }

    private static void print(String expected) {
        System.out.printf("Light is %s. Expected %s. %s\n", light.getColor(), expected, expected.equals(light.getColor()) ? "+" : "-");
    }

    private static void letPassSeconds(int n) {
        for (int i = 0; i < n; i++) {
            light.clock();
        }
    }

    private static void callForGreen() {
        System.out.println("Pedestrian call!");
        light.submitPedestrianCall();
    }
}
