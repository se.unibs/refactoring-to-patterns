package it.unibs.ing.se.refactoring.retryandtrace;

import java.security.spec.ECField;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class Execution {
    private static int lastId = 0;
    private final int id;
    private final ZonedDateTime startedAt;
    private ZonedDateTime completedAt;

    public Execution(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
        id = ++lastId;
    }

    public void completeWithError() {
        completedAt = ZonedDateTime.now();
        System.out.printf("Execution %d failed in %d ms\n", id, differenceInMillis(startedAt, completedAt));
    }

    public void complete() {
        completedAt = ZonedDateTime.now();
        System.out.printf("Execution %d completed successfully in %d ms\n", id, differenceInMillis(startedAt, completedAt));
    }

    private long differenceInMillis(ZonedDateTime from, ZonedDateTime to) {
        return ChronoUnit.MILLIS.between(from, to);
    }

    public long executionTime() {
        return differenceInMillis(startedAt, completedAt);
    }
}
